//////
//	Date: 2019-04-01
//	Description:
//		(a) GitBucket project: \StrategyKgiAfl\
//		(b) Add "shapeHollowCircle" notation on Settlement Date 
//		(c) History data ref: https://www.macroption.com/vix-expiration-calendar/#2019
//////

//--Regulare plot price chart
SetChartOptions(0,chartShowArrows|chartShowDates);
_N(Title = StrFormat("{{NAME}} - {{INTERVAL}} {{DATE}} Open %g, Hi %g, Lo %g, Close %g (%.1f%%) {{VALUES}}", O, H, L, C, SelectedValue( ROC( C, 1 ) ) ));
Plot( C, "Close", ParamColor("Color", colorDefault ), styleNoTitle | ParamStyle("Style") | GetPriceStyle() ); 

/*
You will notice that some months are missing in the early years.
This is not a mistake. 
Full monthly expiration cycle starts in October 2005 (the last gap is September 2005).
*/
//--Build VIX futures Settlement Date
Shape2004 = (DateNum() == 1041117 OR
			 DateNum() == 1041013 OR
			 DateNum() == 1040915 OR
			 DateNum() == 1040818 OR
			 DateNum() == 1040714 OR
			 DateNum() == 1040616 OR
			 DateNum() == 1040519   ) * shapeHollowCircle;

Shape2005 = (DateNum() == 1051221 OR
			 DateNum() == 1051116 OR
			 DateNum() == 1051019 OR
			 DateNum() == 1050817 OR
			 DateNum() == 1050615 OR
			 DateNum() == 1050518 OR
			 DateNum() == 1050316 OR
			 DateNum() == 1050216 OR
			 DateNum() == 1050119   ) * shapeHollowCircle;

Shape2006 = (DateNum() == 1061220 OR
			 DateNum() == 1061115 OR
			 DateNum() == 1061018 OR
			 DateNum() == 1060920 OR
			 DateNum() == 1060816 OR
			 DateNum() == 1060719 OR
			 DateNum() == 1060621 OR
			 DateNum() == 1060517 OR
			 DateNum() == 1060419 OR
			 DateNum() == 1060322 OR
			 DateNum() == 1060215 OR
			 DateNum() == 1060118   ) * shapeHollowCircle;

Shape2007 = (DateNum() == 1071219 OR
			 DateNum() == 1071121 OR
			 DateNum() == 1071017 OR
			 DateNum() == 1070919 OR
			 DateNum() == 1070822 OR
			 DateNum() == 1070718 OR
			 DateNum() == 1070620 OR
			 DateNum() == 1070516 OR
			 DateNum() == 1070418 OR
			 DateNum() == 1070321 OR
			 DateNum() == 1070214 OR
			 DateNum() == 1070117   ) * shapeHollowCircle;

Shape2008 = (DateNum() == 1081217 OR
			 DateNum() == 1081119 OR
			 DateNum() == 1081022 OR
			 DateNum() == 1080917 OR
			 DateNum() == 1080820 OR
			 DateNum() == 1080716 OR
			 DateNum() == 1080618 OR
			 DateNum() == 1080521 OR
			 DateNum() == 1080416 OR
			 DateNum() == 1080319 OR
			 DateNum() == 1080219 OR
			 DateNum() == 1080116   ) * shapeHollowCircle;

Shape2009 = (DateNum() == 1091216 OR
			 DateNum() == 1091118 OR
			 DateNum() == 1091021 OR
			 DateNum() == 1090916 OR
			 DateNum() == 1090819 OR
			 DateNum() == 1090722 OR
			 DateNum() == 1090617 OR
			 DateNum() == 1090520 OR
			 DateNum() == 1090415 OR
			 DateNum() == 1090318 OR
			 DateNum() == 1090218 OR
			 DateNum() == 1090121   ) * shapeHollowCircle;

Shape2010 = (DateNum() == 1101222 OR
			 DateNum() == 1101117 OR
			 DateNum() == 1101020 OR
			 DateNum() == 1100915 OR
			 DateNum() == 1100818 OR
			 DateNum() == 1100721 OR
			 DateNum() == 1100616 OR
			 DateNum() == 1100519 OR
			 DateNum() == 1100421 OR
			 DateNum() == 1100317 OR
			 DateNum() == 1100217 OR
			 DateNum() == 1100120   ) * shapeHollowCircle;

Shape2011 = (DateNum() == 1111221 OR
			 DateNum() == 1111116 OR
			 DateNum() == 1111019 OR
			 DateNum() == 1110921 OR
			 DateNum() == 1110817 OR
			 DateNum() == 1110720 OR
			 DateNum() == 1110615 OR
			 DateNum() == 1110518 OR
			 DateNum() == 1110420 OR
			 DateNum() == 1110316 OR
			 DateNum() == 1110216 OR
			 DateNum() == 1110119   ) * shapeHollowCircle;

Shape2012 = (DateNum() == 1121219 OR
			 DateNum() == 1121121 OR
			 DateNum() == 1121017 OR
			 DateNum() == 1120919 OR
			 DateNum() == 1120822 OR
			 DateNum() == 1120718 OR
			 DateNum() == 1120620 OR
			 DateNum() == 1120516 OR
			 DateNum() == 1120418 OR
			 DateNum() == 1120321 OR
			 DateNum() == 1120215 OR
			 DateNum() == 1120118   ) * shapeHollowCircle;

Shape2013 = (DateNum() == 1131218 OR
			 DateNum() == 1131120 OR
			 DateNum() == 1131016 OR
			 DateNum() == 1130918 OR
			 DateNum() == 1130821 OR
			 DateNum() == 1130717 OR
			 DateNum() == 1130619 OR
			 DateNum() == 1130522 OR
			 DateNum() == 1130417 OR
			 DateNum() == 1130320 OR
			 DateNum() == 1130213 OR
			 DateNum() == 1130116   ) * shapeHollowCircle;

Shape2014 = (DateNum() == 1141217 OR
			 DateNum() == 1141119 OR
			 DateNum() == 1141022 OR
			 DateNum() == 1140917 OR
			 DateNum() == 1140820 OR
			 DateNum() == 1140716 OR
			 DateNum() == 1140618 OR
			 DateNum() == 1140521 OR
			 DateNum() == 1140416 OR
			 DateNum() == 1140318 OR
			 DateNum() == 1140219 OR
			 DateNum() == 1140122   ) * shapeHollowCircle;

Shape2015 = (DateNum() == 1151216 OR
			 DateNum() == 1151118 OR
			 DateNum() == 1151021 OR
			 DateNum() == 1150916 OR
			 DateNum() == 1150819 OR
			 DateNum() == 1150722 OR
			 DateNum() == 1150617 OR
			 DateNum() == 1150520 OR
			 DateNum() == 1150415 OR
			 DateNum() == 1150318 OR
			 DateNum() == 1150218 OR
			 DateNum() == 1150121   ) * shapeHollowCircle;

Shape2016 = (DateNum() == 1161221 OR
			 DateNum() == 1161116 OR
			 DateNum() == 1161019 OR
			 DateNum() == 1160921 OR
			 DateNum() == 1160817 OR
			 DateNum() == 1160720 OR
			 DateNum() == 1160615 OR
			 DateNum() == 1160518 OR
			 DateNum() == 1160420 OR
			 DateNum() == 1160316 OR
			 DateNum() == 1160217 OR
			 DateNum() == 1160120   ) * shapeHollowCircle;

Shape2017 = (DateNum() == 1171220 OR
			 DateNum() == 1171115 OR
			 DateNum() == 1171018 OR
			 DateNum() == 1170920 OR
			 DateNum() == 1170816 OR
			 DateNum() == 1170719 OR
			 DateNum() == 1170621 OR
			 DateNum() == 1170517 OR
			 DateNum() == 1170419 OR
			 DateNum() == 1170322 OR
			 DateNum() == 1170215 OR
			 DateNum() == 1170118   ) * shapeHollowCircle;

Shape2018 = (DateNum() == 1181219 OR
			 DateNum() == 1181121 OR
			 DateNum() == 1181017 OR
			 DateNum() == 1180919 OR
			 DateNum() == 1180822 OR
			 DateNum() == 1180718 OR
			 DateNum() == 1180620 OR
			 DateNum() == 1180516 OR
			 DateNum() == 1180418 OR
			 DateNum() == 1180321 OR
			 DateNum() == 1180214 OR
			 DateNum() == 1180117   ) * shapeHollowCircle;

Shape2019 = (DateNum() == 1191218 OR
			 DateNum() == 1191120 OR
			 DateNum() == 1191016 OR
			 DateNum() == 1190918 OR
			 DateNum() == 1190821 OR
			 DateNum() == 1190717 OR
			 DateNum() == 1190619 OR
			 DateNum() == 1190522 OR
			 DateNum() == 1190417 OR
			 DateNum() == 1190319 OR
			 DateNum() == 1190213 OR
			 DateNum() == 1190116   ) * shapeHollowCircle;			 
			 
			 
PlotShapes( Shape2004, colorRed, 0, H*1.01);			 
PlotShapes( Shape2005, colorRed, 0, H*1.01);
PlotShapes( Shape2006, colorRed, 0, H*1.01);
PlotShapes( Shape2007, colorRed, 0, H*1.01);
PlotShapes( Shape2008, colorRed, 0, H*1.01);

PlotShapes( Shape2009, colorRed, 0, H*1.01);
PlotShapes( Shape2010, colorRed, 0, H*1.01);
PlotShapes( Shape2011, colorRed, 0, H*1.01);
PlotShapes( Shape2012, colorRed, 0, H*1.01);
PlotShapes( Shape2013, colorRed, 0, H*1.01);

PlotShapes( Shape2014, colorRed, 0, H*1.01);
PlotShapes( Shape2015, colorRed, 0, H*1.01);
PlotShapes( Shape2016, colorRed, 0, H*1.01);
PlotShapes( Shape2017, colorRed, 0, H*1.01);
PlotShapes( Shape2018, colorRed, 0, H*1.01);

PlotShapes( Shape2019, colorRed, 0, H*1.01);


//--Control Bottom-Top chart space
GraphXSpace = 5;
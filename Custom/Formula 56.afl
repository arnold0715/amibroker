//////
//	Date: 2019-07-02
//	Description:
//		(a) GitBucket project: \StrategyKgiAfl\PairKudos\
//		(b) Pairs trading strategy
//		(c) 3*ES with 1*NQ
//
//
//////

//----------------------------------------------------------------
//	System Setting & Trade Mode & Header(function) Reference
//----------------------------------------------------------------

//--Header (or function library) include 
#include "D:\\amibroker\\Function\\KudosLib.afl"

OptimizerSetEngine( "cmae" );
SetChartOptions( 0, chartShowDates );
SetOption( "InitialEquity", 200000 );
SetOption( "FuturesMode", True );
SetOption( "CommissionMode", 3 ); // $ per share/contract
SetOption( "CommissionAmount", TickSize * 5 * PointValue + 10 ); // 31.25
//SetOption("AllowSameBarExit", True);
SetOption( "ActivateStopsImmediately", True ); // Let stopModePercent work into intraday.
RoundLotSize = 1;  // Integer contract number. Ex: No 0.9 contract Position.
SetPositionSize( 1, spsShares );
SetOption( "ExtraColumnsLocation", 1 ); // Let Optimize variables change to first column.

Equity( 1, 0 ); // evaluate stops, all quotes; Scan Signal with ApplyStop()

//------------------------------------------------
//	Strategy & Algorithm & Model
//------------------------------------------------
SetTradeDelays( 1, 1, 1, 1 );
EntryMode = Optimize( "EntryMode", 1, 0, 1, 1 );  // [1 = Open], [0 = Close]
if( EntryMode ) {
	BuyPrice = Open;
	SellPrice = Open;
	ShortPrice = Open;
	CoverPrice = Open;
} else {
	BuyPrice = Close;
	SellPrice = Close;
	ShortPrice = Close;
	CoverPrice = Close;
}

//--These are the two issue in the pair
Issue1Ticker = "$SPX";
Issue2Ticker = "$NDX";

//--Close ARRAYs
SetForeign( Issue1Ticker );
Issue1C = C;
RestorePriceArrays();

SetForeign( Issue2Ticker );
Issue2C = C;
RestorePriceArrays();

//--Contract Value Difference
MarketDiff = ( Issue1C * 50 * 1 - Issue2C * 20 * 1 ) + 30000;
TradeLine = ( Issue1C * 3 - Issue2C * 0.4 ) + 0;

PctC = ( MarketDiff - Ref( MarketDiff, -1 ) ) / Ref( MarketDiff, -1 );
PercentLBPeriod = Optimize( "LookBack", 85, 60, 120, 5 );

//--[Cutoff=33] will create three equal-sized groups.
Cutoff = Optimize( "Group", 24, 18, 60, 3 );
TopGroup = PctC >= Percentile( PctC, PercentLBPeriod, 100 - Cutoff );
BottomGroup = PctC <= Percentile( PctC, PercentLBPeriod, Cutoff );
MiddleGroup = ( NOT TopGroup ) AND ( NOT BottomGroup );

//--Set a single variable with the group category of the bar.
Position = IIf( TopGroup, 3, IIf( middleGroup, 2, 1 ) );

//--Form a single identifier for each bar for its three day sequence.
Sequence = 100 * Ref( Position, -2 ) + 10 * Ref( Position, -1 ) + Position;

//--Optimizing area
TwoDaysAgo = Optimize( "TwoDaysAgo", 2, 1, 3, 1 );
OneDayAgo = Optimize( "OneDayAGo", 1, 1, 3 , 1 );
ThisDay = Optimize( "ThisDay", 1, 1, 3, 1 );

Selected = 100 * TwoDaysAgo + 10 * OneDayAgo + ThisDay;


EntrySignal = ( Selected == Sequence );
ExitSignal = 0;



EsSize = Optimize( "EsSize", 3, 1, 3, 1 );
NqSize = Optimize( "NqSize", 1, 1, 3, 1 );


//--Optimize Long Short Mode
LSMode = Optimize( "LSMode", 1, 0, 1, 1 );  // [1 = Long Issue1 Short Issue2], [0 = Short Issue1 Long Issue2]

//--For execution pair-trade setting
if( Name() == Issue1Ticker )
{
	SetPositionSize( EsSize, spsShares );
	if( LSMode ) {
		Buy = EntrySignal;
		Sell = 0;
	} else {
		Short = EntrySignal;
		Cover = 0;
	}
}

if( Name() == Issue2Ticker )
{
	SetPositionSize( NqSize, spsShares );
	if( LSMode ) {
		Short = EntrySignal;
		Cover = 0;
	} else {
		Buy = EntrySignal;
		Sell = 0;
	}
}


//--N-bar stop
HoldDays = Optimize( "HoldDays", 14, 4, 15, 1 );
ApplyStop( stopTypeNBar, stopModeBars, HoldDays - 3 );  

//------------------------------------------------
//	Custom Plot 
//------------------------------------------------
StrategyName = "Pair ES with NQ - 0003";
_N(Title = StrFormat(EncodeColor( colorRed ) + StrategyName + 
					EncodeColor( colorBlack ) + "{{NAME}} - {{INTERVAL}} {{DATE}} Open %g, Hi %g, Lo %g, Close %g (%.1f%%) {{VALUES}}",
					O, H, L, C, SelectedValue( ROC( C, 1 ) ) ));
					

Plot( MarketDiff, EncodeColor( colorRed ) + "MarketDiff", colorRed, styleNoLine | styleThick | styleDots );
Plot( MarketDiff, "", colorBlack, styleLine | styleNoLabel );

Plot( TradeLine, EncodeColor( colorBlue ) + "TradeLine", colorBlue, styleLine | styleLeftAxisScale );
Plot( Issue2C, EncodeColor( colorBrown ) + "NQ1", colorBrown, styleDashed | styleOwnScale );
Plot( Issue1C, EncodeColor( colorBlueGrey ) + "ES1", colorBlueGrey, styleDashed | styleOwnScale );



//--plot Buy signal with Triangle style
PlotShapes( EntrySignal*shapeHollowUpTriangle, colorRed, 0, MarketDiff );
PlotShapes( ExitSignal*shapeHollowDownTriangle, colorGreen, 0, MarketDiff );

GraphXSpace = 15;

//------------------------------------------------
//	Explore & Debug & TimeSeries View & DataFrame
//------------------------------------------------

Filter = 1;
//AddSummaryRows( 63, 1.2 ); 

AddColumn( Issue1C, Issue1Ticker, 1.2 );
AddColumn( Issue2C, Issue2Ticker, 1.2 );

AddColumn( MarketDiff, "MarketValueDiff", 1.2 );
AddColumn( PctC, "PctC", 1.3 );
AddColumn( Sequence, "Sequence", 1.0 );
AddColumn( Selected, "Selected", 1.0 );
AddColumn( ATR(20), "Selected", 1.0 );






// The code for AmiBroker 5.50 and above
// YOUR TRADING SYSTEM HERE
// ....
//SetCustomBacktestProc("");
//if( Status("action") == actionPortfolio )
//{
//	bo = GetBacktesterObject();
//	bo.Backtest();
//	AddToComposite( bo.EquityArray,
//	"~MY_EQUITY_Pair_0003", "X",
//	atcFlagDeleteValues | atcFlagEnableInPortfolio );
//}

//////
//	Date: 2019-07-03
//	Description:
//		(a) GitBucket project: \StrategyKgiAfl\RunTimeST
//		(b) [RollingPower + SettlementDate + BasisLine] Strategy
//		(c) Short at open, cover at close.
//		(d) ATR-Filter
//
//////

//----------------------------------------------------------------
//	System Setting & Trade Mode & Header(function) Reference
//----------------------------------------------------------------

//--Header (or function library) include 
#include "D:\\strategykgiafl\\Include\\HeaderVixSettlementDate.afl"

//--This prevents strings to be directly written
EnableTextOutput( False );

SetChartOptions( 0, chartShowDates );
SetOption( "InitialEquity", 100000 );
SetOption( "FuturesMode", True );
SetOption( "CommissionMode", 3 ); // $ per share/contract
SetOption( "CommissionAmount", 2.3 + 2.5 * TickSize * PointValue );
SetOption( "AllowSameBarExit", True );
SetOption( "ActivateStopsImmediately", True ); // Let stopModePercent work into intraday.
RoundLotSize = 1;  // Integer contract number. Ex: No 0.9 contract Position.
SetPositionSize( 1, spsShares );

Equity( 1, 0 ); // evaluate stops, all quotes; Scan Signal with ApplyStop()

//------------------------------------------------
//	Strategy & Algorithm & Model
//------------------------------------------------

SetTradeDelays( 0, 0, 0, 0 );
BuyPrice = Open;
SellPrice = Open;
ShortPrice = Open;
CoverPrice = Close;

//--These are the two issue in use
Issue1Ticker = "VIX";
Issue2Ticker = "UX1";

//--Close ARRAYs
Issue1C = Foreign( Issue1Ticker, "C", 1 );
Issue2C = Foreign( Issue2Ticker, "C", 1 );

//--Calculate Basis = Spot - Futures
BasisLine = Issue1C - Issue2C;

//--Parameters algo trade days.
BeforeDay = Optimize( "Before Settle Day", 3, 1, 10, 1 );
TurnOnShort = BarsSince( Ref( SettleDate, BeforeDay ) ) <= BeforeDay AND !SettleDate;

//--Build [ATR-Filter]
ATRLB = Optimize( "ATRLB", 10, 8, 16, 2 );
ATRValueLowerLimit = Optimize( "ATRValueLowerLimit", 75, 40, 100, 5 );
AtrPrLb = Optimize( "AtrPrLb", 35, 20, 160, 5 );
ATRValue = ATR( ATRLB );
ATRValuePR = Ref( PercentRank( ATRValue, AtrPrLb ), -1 );
TodayATRValuePR = PercentRank( ATRValue, AtrPrLb );
ATRFilter = ATRValuePR >= ATRValueLowerLimit;

Short = TurnOnShort AND ATRFilter;
Cover = TurnOnShort AND ATRFilter;
Short = ExRem( Short, Cover ); 
Cover = ExRem( Cover, Short );

ProfitPercent = Optimize("ProfitPercent", 12, 10, 15, 1);
ApplyStop( stopTypeProfit, stopModePercent, ProfitPercent, True ); 

//------------------------------------------------
//	Custom Plot 
//------------------------------------------------
StrategyName = "ShortVixSettlement - ";
_N(Title = StrFormat(EncodeColor( colorRed ) + StrategyName + 
					EncodeColor( colorBlack ) + "{{NAME}} - {{INTERVAL}} {{DATE}} Open %g, Hi %g, Lo %g, Close %g (%.1f%%) {{VALUES}}",
					O, H, L, C, SelectedValue( ROC( C, 1 ) ) ));
Plot( Close, "", colorBlack, styleCandle );
PlotShapes( SettleDate*shapeHollowCircle, colorRed, 0, H*1.01 );	
Plot( TodayATRValuePR, "TodayATRValuePR", colorBlue, styleLeftAxisScale | styleDashed );
Plot( 75, "", colorRed, styleLeftAxisScale | styleThick );
//--plot Short signal with Hollow style
//PlotShapes( Short*shapeHollowDownTriangle, colorRed, 0, High );
//PlotShapes( Cover*shapeHollowUpTriangle, colorGreen, 0, Low );

//------------------------------------------------
//	Explore & Debug & TimeSeries View & DataFrame
//------------------------------------------------

Filter = 1;

AddColumn( Issue1C, "VIX" , 1.2 );
AddColumn( Issue2C, "UX1" , 1.2 );

ColorBasis = IIf( BasisLine >= 0, colorRed, colorBlack );

AddColumn( BasisLine, "Basis", 1.2,  ColorBasis );


ColorSettle = IIf( SettleDate == 1, colorRed, colorBlack );
ColorSettleBg = IIf( SettleDate == 1, colorYellow, colorDefault );
AddColumn( SettleDate, "Settle Date", 1.0, ColorSettle, ColorSettleBg );
AddColumn( TurnOnShort, "TurnOnShort", 1.0 );

AddColumn( ATRValuePR, "ATRValuePR", 1.0 );
AddColumn( TodayATRValuePR, "Today ATRValuePR", 1.0 );


//--Short Display Area
AddColumn( Short, "Short" , 1 );
AddColumn( Cover, "Cover", 1 );





